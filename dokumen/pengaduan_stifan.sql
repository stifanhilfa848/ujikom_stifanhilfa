-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 14 Apr 2022 pada 07.01
-- Versi server: 10.1.36-MariaDB
-- Versi PHP: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pengaduan_masyarakat`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `masyarakat`
--

CREATE TABLE `masyarakat` (
  `nik` char(16) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(100) NOT NULL,
  `telp` varchar(13) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `masyarakat`
--

INSERT INTO `masyarakat` (`nik`, `nama`, `username`, `password`, `telp`, `image`) VALUES
('121314', 'erenyeager', 'eren', '$2y$10$KLjQ2QULx3q1b1sMuHXjTeEzNR0PZawC1i.mF2MXZDUio/OOoKM2u', '089765487655', '715a0e7738af81f07a2e16b9fc748cc0.png'),
('1412', 'stifan', 'ipan', '1412', '08987654320', 'default.png'),
('454635', 'fakir', 'miskin', '$2y$10$8A4vf3cV/a6Xck09L3OujuQz05OsCytJgUxlKkjVncfcbLGwT5yHC', '0897865432', 'e40e9467b0d39e7bf7b729ab15feebb3.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengaduan`
--

CREATE TABLE `pengaduan` (
  `id_pengaduan` int(11) NOT NULL,
  `tgl_pengaduan` date NOT NULL,
  `nik` char(16) NOT NULL,
  `isi_laporan` text NOT NULL,
  `foto` varchar(255) NOT NULL,
  `status` enum('0','proses','selesai') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengaduan`
--

INSERT INTO `pengaduan` (`id_pengaduan`, `tgl_pengaduan`, `nik`, `isi_laporan`, `foto`, `status`) VALUES
(1, '2022-04-05', '454635', 'dsdsdsdsd', '9c87f144b7595e2a5e045ec1b8bb8bf7.png', ''),
(2, '2022-04-05', '454635', 'patrick rusuh', '814c3776746e961401243d009512fee0.jpg', ''),
(1243, '2022-01-02', '1412', 'aku di rampok di bank bni', 'bni', 'selesai'),
(1244, '2022-04-06', '454635', 'sdfsdfsdfsdf', '67e889f172630e3022ec7685011b076c.jpg', ''),
(1245, '2022-04-06', '454635', 'aku anak sehat tubuh ku kuat', 'ac16c78d4144c3d01e7021504325aa62.jpg', 'selesai'),
(1246, '2022-04-06', '121314', 'rumbling hela ngab', 'efd346cbe3c75ab93b132e506b1eaacf.jpg', 'selesai'),
(1251, '2022-04-12', '121314', 'dfsdfsdfdsfsd', '5f329255a19d39a8ae4a93e18de4ae72.jpg', 'selesai'),
(1252, '2022-04-12', '121314', 'adaaa adaaa aja dek', '2dce7f430c750692044b6777d66435a8.png', ''),
(1253, '2022-04-14', '121314', 'apa a', '04ce6f88bcc86391c2e4934d7e94b350.png', '0'),
(1254, '2022-04-14', '121314', 'ada ada dek', 'a41edba550be0ced1bc4388c2cd3efde.jpg', '0'),
(1255, '2022-04-14', '121314', 'ada ke kacauan di angkasa', '57ca61eb8dea725ef62c1d1d66e44ab5.jpg', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL,
  `nama_petugas` varchar(35) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(64) NOT NULL,
  `telp` varchar(13) NOT NULL,
  `level` enum('admin','petugas') NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `nama_petugas`, `username`, `password`, `telp`, `level`, `image`) VALUES
(1, 'gustian', 'rekt', '$2y$10$ToGO5G52IheasRvF0dsQ2.T1WPCbQhjeI0YLlO.TOzdlAWXsu3AtO', '465465', 'admin', 'images.png'),
(1351, 'stifan', 'stifan', '$2y$10$myPhawwJOlZhJT6qfCfuxeXNemFt41rwBRk7FQ.ScO3B3ffhkq3su', '0896547120', 'petugas', 'spongebob.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tanggapan`
--

CREATE TABLE `tanggapan` (
  `id_tanggapan` int(11) NOT NULL,
  `id_pengaduan` int(11) NOT NULL,
  `tgl_tanggapan` date NOT NULL,
  `tanggapan` text NOT NULL,
  `id_petugas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tanggapan`
--

INSERT INTO `tanggapan` (`id_tanggapan`, `id_pengaduan`, `tgl_tanggapan`, `tanggapan`, `id_petugas`) VALUES
(1, 1246, '2022-04-12', 'hshashahsahsa', 1),
(56789, 1243, '2022-01-12', 'lebih berhati hati lagi yagesya', 1345),
(56790, 1245, '2022-04-12', 'sdsdsdas', 1),
(56791, 1, '2022-04-12', 'aneh', 1),
(56792, 2, '2022-04-12', 'gajelas dek dek', 1),
(56793, 1244, '2022-04-12', 'dek dek', 1),
(56794, 1248, '2022-04-12', 'ya terus kenapa', 1),
(56795, 1249, '2022-04-12', 'ya terus kenapa dek', 1),
(56796, 1250, '2022-04-12', 'y', 1),
(56797, 1251, '2022-04-12', 'iya apa', 1),
(56798, 1252, '2022-04-14', 'apasi', 1351),
(56799, 1255, '2022-04-14', 'apasi gj', 1);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `masyarakat`
--
ALTER TABLE `masyarakat`
  ADD PRIMARY KEY (`nik`);

--
-- Indeks untuk tabel `pengaduan`
--
ALTER TABLE `pengaduan`
  ADD PRIMARY KEY (`id_pengaduan`),
  ADD KEY `nik` (`nik`);

--
-- Indeks untuk tabel `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`);

--
-- Indeks untuk tabel `tanggapan`
--
ALTER TABLE `tanggapan`
  ADD PRIMARY KEY (`id_tanggapan`),
  ADD KEY `id_petugas` (`id_petugas`),
  ADD KEY `id_pengaduan` (`id_pengaduan`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `pengaduan`
--
ALTER TABLE `pengaduan`
  MODIFY `id_pengaduan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1256;

--
-- AUTO_INCREMENT untuk tabel `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id_petugas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1352;

--
-- AUTO_INCREMENT untuk tabel `tanggapan`
--
ALTER TABLE `tanggapan`
  MODIFY `id_tanggapan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56800;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `pengaduan`
--
ALTER TABLE `pengaduan`
  ADD CONSTRAINT `pengaduan_ibfk_1` FOREIGN KEY (`nik`) REFERENCES `masyarakat` (`nik`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
