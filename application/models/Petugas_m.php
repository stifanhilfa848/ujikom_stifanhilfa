<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Petugas_m extends CI_Model {

	private $table = 'petugas';
	private $primary_key = 'id_petugas';

	public function create($data)
	{
		return $this->db->insert($this->table, $data);
	}	
	public function update($username, $data)
	{
		if (!$username) {
			return;
		}
		$this->db->set($data);
		$this->db->where('username',$username);
		return $this->db->update('petugas');
		// return $this->db->update($this->_table, $data, ['username' => $username]);
	}
	
	public function select_petugas($username)
	{
		if (!$username) {
			return;
		}
		return $this->db->get_where('petugas',['username' => $username])->row_array();
		// return $this->db->update($this->_table, $data, ['username' => $username]);
	}
}

/* End of file Petugas_m.php */
/* Location: ./application/models/Petugas_m.php */