<?php
defined('BASEPATH') or exit('No direct script access allowed');

class LoginController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Petugas_m');
    }
    public function index()
    {   
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        if($this->form_validation->run() == false){
        $data['title'] = 'Login page';
        $this->load->view('templates/auth_header');
        $this->load->view('Admin/login_admin');
        $this->load->view('templates/auth_footer');
        }else{
            $this->login_admin();
        }
    }
    private function login_admin()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $petugas = $this->db->get_where('petugas', ['username' => $username])->row_array();

        // jika petugasnya ada
        if ($petugas) {
            // jika akun petugas == TRUE
            // cek password
            if (password_verify($password, $petugas['password'])) {
                // jika password benar
                // maka buat session userdata
                $session = [
                    'username'         => $petugas['username'],
                    'level'            => $petugas['level'],
                ];
                $this->session->set_userdata($session);
                if ($petugas['level'] == 'admin') {
                    return redirect('Admin/DashboardController');
                } elseif ($petugas['level'] == 'petugas')
                    return redirect('Admin/DashboardController');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">password salah!</div>');
                redirect('Admin/LoginController');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">username tidak terdaftar</div>');
            redirect('Admin/LoginController');
        }
    }
    
    public function registration()
    {
        $this->form_validation->set_rules('nik', 'Nik', 'required|trim|is_unique[masyarakat.nik]',[
            'is_unique' => 'Nik sudah terdaftar'
        ]);    
        
        $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
        $this->form_validation->set_rules('telp', 'Telp', 'required|trim');
        $this->form_validation->set_rules('username', 'Usernaname', 'required|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');

        if($this->form_validation->run() == false){
           $data['title'] = 'User Registration';
           $this->load->view('templates/auth_header', $data);
           $this->load->view('auth/registration');
           $this->load->view('templates/auth_footer');
        } else {
            $data = [
                'nik' => htmlspecialchars($this->input->post('nik')),
                'nama' => htmlspecialchars($this->input->post('nama')),
                'telp' => $this->input->post('telp'),
                'username' => ($this->input->post('username')),
                'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT)

                
            ];

            $this->db->insert('masyarakat', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">SELAMAT!anda sudah terdaftar silahkan login</div>');
            redirect('auth');}


        
    }

    public function registration_petugas()
	{
		$data['title'] = 'Tambah Petugas';
		$data['data_petugas'] = $this->db->get('petugas')->result_array();

		$this->form_validation->set_rules('nama','Nama','trim|required|alpha_numeric_spaces');
		$this->form_validation->set_rules('username','Username','trim|required|alpha_numeric_spaces');
		$this->form_validation->set_rules('password','Password','trim|required|alpha_numeric_spaces|min_length[6]|max_length[15]');
		$this->form_validation->set_rules('telp','Telp','trim|required|numeric');
		$this->form_validation->set_rules('level','Level','trim|required');

		if ($this->form_validation->run() == FALSE) :
            $data['title'] = 'User Registration';
            $this->load->view('templates/auth_header');
            $this->load->view('admin/registration_petugas',$data);
            $this->load->view('templates/auth_footer');
		else :
			$params = [
				'nama_petugas'			=> htmlspecialchars($this->input->post('nama',TRUE)),
				'username'				=> htmlspecialchars($this->input->post('username',TRUE)),
				'password'				=> password_hash(htmlspecialchars($this->input->post('password',TRUE)), PASSWORD_DEFAULT),
				'telp'					=> htmlspecialchars($this->input->post('telp',TRUE)),
				'level'					=> htmlspecialchars($this->input->post('level',TRUE)),
			];

			$resp = $this->Petugas_m->create($params);

			if ($resp) :
				$this->session->set_flashdata('message','<div class="alert alert-primary" role="alert">
					Buat akun petugas berhasil
					</div>');

                    redirect('Auth');
			else :
				$this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
					Buat akun petugas berhasil!
					</div>');

                    redirect('Auth');
			endif;
		endif;
	}
    public function username_check($str = NULL)
    {
        if (!empty($str)) :
            $masyarakat = $this->db->get_where('masyarakat', ['username' => $str])->row_array();

            $petugas = $this->db->get_where('petugas', ['username' => $str])->row_array();

            if ($masyarakat == TRUE or $petugas == TRUE) :

                $this->form_validation->set_message('username_check', 'Username ini sudah terdaftar ada.');

                return FALSE;
            else :
                return TRUE;
            endif;
        else :
            $this->form_validation->set_message('username_check', 'Inputan Kosong');

            return FALSE;
        endif;
    }
    public function logout()
    {
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('password');

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">ANDA TELAH LOGOUT!</div>');
            redirect('admin/LoginController');}


    }
    

