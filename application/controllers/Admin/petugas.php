<?php

use LDAP\Result;

defined('BASEPATH') or exit('No direct script access allowed');

class Petugas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('auth_model');
        $this->load->model('profile_model');
        $this->load->model('Pengaduan_m');
        $this->load->model('Petugas_m');
    }

    public function index()
    {

        $data['petugas'] = $this->db->get_where('petugas', ['username' =>
        $this->session->userdata('username')])->row_array();

        $this->load->view('_part/backend_sidebar_v', $data);
    }

    public function profile()
    {
        $data['title'] = 'Profile';

        $petugas = $this->db->get_where('petugas',['username' => $this->session->userdata('username')])->row_array();

        if ($petugas == TRUE) {
			$data['admin'] = $petugas;
        }
        $this->load->view('templates/auth_header');
        $this->load->view('templates/auth_footer');
        $this->load->view('admin/profile_utama', $data);
    }
    public function change_image()
    {
        $data['title'] = 'Profile';
        $petugas = $this->db->get_where('petugas',['username' => $this->session->userdata('username')])->row_array();

        $this->load->view('templates/auth_header');
        $this->load->view('templates/auth_footer');
        $this->load->view('admin/profile', $data);
    }
    public function upload_image()
    {
        $this->load->model('profile_model');
        $this->load->model('auth_model');
        $data['current_user'] = $this->auth_model->current_user();
        $username = $this->session->userdata('username');
        $image = $this->session->userdata('image');

        $file_name = str_replace('.','',$data['current_user']->id);
        $config['upload_path']          = realpath(APPPATH . '../assets/img/profile/');
        $config['allowed_types']        = 'gif|jpg|jpeg|png';
        $config['file_name']            = $file_name;
        $config['overwrite']            = true;
        $config['max_size']             = 1024; // 1MB
        $config['max_width']            = 1024;
        $config['max_height']           = 1024;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('avatar')) {
            $data['error'] = $this->upload->display_errors();
            var_dump($data['error']);
        } else {
            $uploaded_data = $this->upload->data();

            $new_data = [
                'image' => $uploaded_data['file_name'],
            ];

            if ($this->Petugas_m->update($username,$new_data)) {
                $this->session->set_flashdata('message', 'Avatar was updated');
                redirect(site_url('admin/petugas/profile'));
            }
        }
    }
    public function ganti_password()
    {
        $data['title'] = 'Ganti Password';
        $this->load->library('form_validation');
        $this->load->model('profile_model');
        $data['current_user'] = $this->session->userdata('username');
       
        if ($this->input->method() === 'post') {
            $rules = $this->profile_model->password_rules();
            $this->form_validation->set_rules($rules);

            if ($this->form_validation->run() === FALSE) {
                return $this->load->view('user/ganti_password.php', $data);
            }

            $new_password_data = [
                'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
            ];
            
            if ($this->Petugas_m->update($data['current_user'], $new_password_data)) {
                $this->session->set_flashdata('message', 'Password was changed');
                redirect(site_url('admin/petugas/ganti_password'));
            }
        }

        $this->load->view('admin/ganti_password.php', $data);
    }
    public function remove_image()
    {
        $username = $this->session->userdata('username');
        $petugas = $this->Petugas_m->select_petugas($username);
        
        // hapus file
        $file_name = $petugas['image'];
        
       
        $data = [
            'image' => 'user.png',
        ];
        if ($this->Petugas_m->update($username,$data)) {
            $this->session->set_flashdata('message', 'Avatar dihapus!');
            redirect(site_url('admin/petugas/profile'));
        }
    }
}
