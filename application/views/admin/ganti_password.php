<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Tulis Pengaduan</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-info sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url('Admin/DashboardController'); ?>">
                <div class="sidebar-brand-icon">
                    <button class="rounded-circle border-0" id="sidebarToggle"></button>
                </div>
                <div class="sidebar-brand-text mx-3">Kembali</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->


            <!-- Divider -->






        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->

                <!-- End of Topbar -->
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <br></br>
                    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

                    <?= validation_errors('<div class="alert alert-danger alert-dismissible fade show" role="alert">', '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	</div>') ?>
                    <?= $this->session->flashdata('message'); ?>

                    <!-- Carousel -->
                    <div id="carouselExampleFade" class="carousel slide carousel-fade bg-light mb-4" data-ride="carousel">
                        <div class="container">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <div class="row d-flex">
                                        <div class="col-lg-5">
                                            <main class="main">
                                                <?php $this->load->view('templates/auth_header') ?>
                                                <div class="content">
                                                    <form action="" method="POST">
                                                        <div>
                                                            <label for="password">Password Baru</label>
                                                            <input type="password" name="password" class="<?= form_error('password') ? 'invalid' : '' ?>" value="<?= set_value("password") ?>" required />
                                                            <div class="invalid-feedback">
                                                                <?= form_error('password') ?>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <label for="password_confirm">Konfirmasi Password Baru</label>
                                                            <input type="password" name="password_confirm" class="<?= form_error('password_confirm') ? 'invalid' : '' ?>" value="<?= set_value("password_confirm") ?>" required />
                                                            <div class="invalid-feedback">
                                                                <?= form_error('password_confirm') ?>
                                                            </div>
                                                        </div>

                                                        <div>
                                                            <button type="submit" name="save" class="button button-primary">Save Password</button>
                                                        </div>
                                                    </form>

                                                    <?php $this->load->view('templates/auth_footer.php') ?>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Carousel -->

                        <!-- Bootstrap core JavaScript-->
                        <script src="vendor/jquery/jquery.min.js"></script>
                        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

                        <!-- Core plugin JavaScript-->
                        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

                        <!-- Custom scripts for all pages-->
                        <script src="js/sb-admin-2.min.js"></script>

                        <!-- Page level plugins -->
                        <script src="vendor/chart.js/Chart.min.js"></script>

                        <!-- Page level custom scripts -->
                        <script src="js/demo/chart-area-demo.js"></script>
                        <script src="js/demo/chart-pie-demo.js"></script>

</body>

</html>