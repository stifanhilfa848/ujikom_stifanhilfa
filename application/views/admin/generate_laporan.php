<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Tulis Pengaduan</title>

	<!-- Custom fonts for this template-->
	<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

	<!-- Custom styles for this template-->
	<link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

	<!-- Page Wrapper -->
	<div id="wrapper">

		<!-- Sidebar -->
		<ul class="navbar-nav bg-gradient-info sidebar sidebar-dark accordion" id="accordionSidebar">

			<!-- Sidebar - Brand -->
			<a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url('Admin/DashboardController'); ?>">
				<div class="sidebar-brand-icon">
					<button class="rounded-circle border-0" id="sidebarToggle"></button>
				</div>
				<div class="sidebar-brand-text mx-3">Kembali</div>
			</a>

			<!-- Divider -->
			<hr class="sidebar-divider my-0">

			<!-- Nav Item - Dashboard -->


			<!-- Divider -->






		</ul>
		<!-- End of Sidebar -->

		<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">

			<!-- Main Content -->
			<div id="content">

				<!-- Topbar -->

				<!-- End of Topbar -->

				<!-- Begin Page Content -->
				<div class="container-fluid">

<!-- Page Heading -->
<br></br>

<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->
	<h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

	<?php if ($laporan) : ?>
		<table class="table">
			<thead class="thead-dark">
				<tr>
					<th scope="col">No</th>
					<th scope="col">Nama</th>
					<th scope="col">Nik</th>
					<th scope="col">Laporan</th>
					<th scope="col">Tgl P</th>
					<th scope="col">Status</th>
					<th scope="col">Tanggapan</th>
					<th scope="col">Tgl T</th>
				</tr>
			</thead>
			<tbody>

				<?php $no = 1; ?>
				<?php foreach ($laporan as $l) : ?>
					<tr>
						<th scope="row"><?= $no++; ?></th>
						<td><?= $l['nama'] ?></td>
						<td><?= $l['nik'] ?></td>
						<td><?= $l['isi_laporan'] ?></td>
						<td><?= $l['tgl_pengaduan'] ?></td>
						<td>
							<?php
							if ($l['status'] == '0') {
								?>
								<span class="badge badge-secondary">Sedang di verifikasi</span>
								<?php } elseif ($l['status'] == 'proses') { ?>
									<span class="badge badge-primary">Sedang di proses</span>
								<?php } elseif ($l['status'] == 'selesai') { ?>
									<span class="badge badge-success">Selesai di kerjakan</span>
								<?php } elseif ($l['status'] == 'tolak') { ?>
									<span class="badge badge-danger">Pengaduan di tolak</span>
							<?php }
							?>
						</td>
						<td><?= $l['tanggapan'] == null ? '-' : $l['tanggapan']; ?></td>
						<td><?= $l['tgl_tanggapan'] == null ? '-' : $l['tgl_tanggapan']; ?></td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

		<a target="_blank" href="<?= base_url('Admin/LaporanController/generate_laporan') ?>" class="btn btn-primary mt-2">Preview or Download</a>

	<?php else : ?>
		<p class="text-center">Belum ada data</p>
	<?php endif; ?>
</div>
<!-- /.container-fluid -->