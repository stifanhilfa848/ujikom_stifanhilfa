<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Profile</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-info sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url('Admin/DashboardController'); ?>">
                <div class="sidebar-brand-icon">
                    <button class="rounded-circle border-0" id="sidebarToggle"></button>
                </div>
                <div class="sidebar-brand-text mx-3">Kembali</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->


            <!-- Divider -->






        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->

                <!-- End of Topbar -->


                <!-- Begin Page Content -->
                <div class="container-fluid">

					<!-- Page Heading -->
					<br></br>
					
				<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->
	<h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

	<?= validation_errors('<div class="alert alert-danger alert-dismissible fade show" role="alert">', '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	</div>') ?>
	<?= $this->session->flashdata('message'); ?>

	<?php if (!empty($data_pengaduan)) : ?>

		<div class="row no-gutters">
			<?php foreach ($data_pengaduan as $dp) : ?>
				<div class="col-md-4">
					<div class="card shadow mb-4" style="width: 18rem;">
						<div class="card-header py-3">
							<h6 class="m-0 font-weight-bold text-primary"><?= $dp['nama'] ?></h6>
						</div>
						<img height="150" src="<?= base_url() ?>assets/uploads/<?= $dp['foto'] ?>" class="card-img-top">
						<div class="card-body">
							<span class="text-dark">Laporan :</span>
							<p><?= $dp['isi_laporan'] ?></p>
							<span class="text-dark">Telp :</span>
							<p><?= $dp['telp'] ?></p>
							<span class="text-dark">Tgl Pengaduan :</span>
							<p><?= $dp['tgl_pengaduan'] ?></p>
						</div>
						<div class="text-center mb-2">
							<div class="">

								<?= form_open('Admin/TanggapanController/tanggapan_detail'); ?>
								<input type="hidden" name="id" value="<?= $dp['id_pengaduan'] ?>">
								<button class="btn btn-success" name="terima">Lihat Detail</button>
								<?= form_close(); ?>

							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>

	<?php else : ?>
		<div class="text-center">Belum Ada Pengaduan</div>
	<?php endif; ?>


</div>

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
                <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

                <!-- Core plugin JavaScript-->
                <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

                <!-- Custom scripts for all pages-->
                <script src="js/sb-admin-2.min.js"></script>

                <!-- Page level plugins -->
                <script src="vendor/chart.js/Chart.min.js"></script>

                <!-- Page level custom scripts -->
                <script src="js/demo/chart-area-demo.js"></script>
                <script src="js/demo/chart-pie-demo.js"></script>

</body>

</html>