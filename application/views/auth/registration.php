    <div class="container">
    <div class="card o-hidden border-0 shadow-lg my-5 col-lg-7  mx-auto">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">registration</h1>
                            </div>
                            <form class="user" method="post" action="<?= base_url('auth/registration');?>">
                                 <div class="form-group">
                                    <input type="text" class="form-control form-control-user"  id="nik" name="nik" 
                                    placeholder="Enter your Nik">
                                    <?= form_error('nik', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user" id="nama" name="nama"
                                        placeholder="Enter fullname">
                                        <?= form_error('nama', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>

                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user" id="telp"  name="telp"
                                        placeholder="Enter telephone">
                                        <?= form_error('telp', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>

                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user"  name="username"
                                        placeholder="Enter username">
                                        <?= form_error('username', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                <input type="password" class="form-control form-control-user"  name="password"
                                placeholder="Enter password">
                                <?= form_error('password', '<small class="text-danger pl-3">', '</small>'); ?>
                                 </div>

                                 <div class="form-group">
                                            <input type="password" class="form-control form-control-user" id="pwd" name="password"
                                            placeholder="Confrim password">
                                            <?= form_error('password', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                               

                                <button type="submit" class="btn btn-primary btn-user btn-block">
                                    Register Account
                                </button>

                            </form>
                            <hr>
                            <div class="text-center">
                                <a class="small" href="forgot-password.html">Forgot Password?</a>
                            </div>
                            
                            <div class="text-center">
                                <a class="small" href="<?= base_url('auth'); ?>">Already have an account? Login!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
