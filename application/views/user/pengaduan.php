<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Tulis Pengaduan</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-info sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url('masyarakat'); ?>">
                <div class="sidebar-brand-icon">
                    <button class="rounded-circle border-0" id="sidebarToggle"></button>
                </div>
                <div class="sidebar-brand-text mx-3">Kembali</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->


            <!-- Divider -->






        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->

                <!-- End of Topbar -->
                <!-- Begin Page Content -->
        <div class="container-fluid">

<!-- Page Heading -->
<br></br>
<!-- Begin Page Content -->
<div class="container-fluid">
  <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
  <div>
    <a href="<?= site_url('masyarakat/pengaduan_baru') ?>" class="button button-primary">+ Tulis Pengaduan</a>
  </div>
<!-- Page Heading -->
<h1 class="h3 mb-4 mt-5 text-gray-800">Data Pengaduan</h1>
  
  <div class="table-responsive">
  <table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Isi Laporan</th>
        <th scope="col">Tgl Melapor</th>
        <th scope="col">Foto</th>
        <th scope="col">Status</th>
        <th scope="col">Lihat Detail</th>
        <th scope="col">Aksi</th>
      </tr>
    </thead>
    <tbody>
      <?php $no = 1; ?>
      <?php foreach ($data_pengaduan as $dp) : ?>
        <tr>
          <th scope="row"><?= $no++; ?></th>
          <td><?= $dp['nama']; ?></td>
          <td><?= $dp['isi_laporan']; ?></td>
          <td><?= $dp['tgl_pengaduan']; ?></td>
          <td>
            <img width="100" src="<?= base_url() ?>assets/uploads/<?= $dp['foto']; ?>" alt="">
          </td>
          <td>
                        <?php
                        if ($dp['status'] == '0') {

                        ?>
                          <span class="badge badge-secondary">Sedang di verifikasi</span>
                        <?php } elseif ($dp['status'] == 'proses') { ?>
                          <span class="badge badge-primary">Sedang di proses</span>
                        <?php } elseif ($dp['status'] == 'selesai') { ?>
                          <span class="badge badge-success">Selesai di kerjakan</span>
                        <?php } elseif ($dp['status'] == 'tolak') { ?>
                          <span class="badge badge-danger">Pengaduan di tolak</span>
                        <?php }
                        ?>
                      </td>
          
          <td class="text-center">
            <a href="<?= base_url('Masyarakat/pengaduan_detail/'.$dp['id_pengaduan']) ?>" class="btn btn-success"><i class="fas fa-fw fa-eye"></i></a>
          </td>

          <?php if ($dp['status'] == '0') : ?>
            <td>
              <!-- <a href="<?= base_url('Masyarakat/pengaduan_batal/'.$dp['id_pengaduan']) ?>" class="btn btn-warning">Hapus</a> -->

              <a href="<?= base_url('Masyarakat/edit/'.$dp['id_pengaduan']) ?>" class="btn btn-info">Edit</a>
            </td>

            <?php else : ?>
              <td><small>Tidak ada aksi</small></td>
            <?php endif; ?>


          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    </div>


  </div>
  <!-- /.container-fluid -->
