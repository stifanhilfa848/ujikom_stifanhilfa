
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Tulis Pengaduan</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-info sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url('masyarakat/pengaduan'); ?>">
                <div class="sidebar-brand-icon">
                    <button class="rounded-circle border-0" id="sidebarToggle"></button>
                </div>
                <div class="sidebar-brand-text mx-3">Kembali</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->


            <!-- Divider -->






        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->

                <!-- End of Topbar -->
<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->
	<h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

	<div class="row">

	</div>

	<div class="card mb-3 col-lg-8">
		<div class="row no-gutters">
			<div class="col-md-4 mt-2 ml-2">
				<img src="<?= base_url('assets/uploads/'.$data_pengaduan['foto']) ?>" class="card-img" alt="img user">
			</div>
			<div class="col-md-8">
				<div class="card-body">
					<h5 class="card-title">Laporan : <span class="text-dark"><?= $data_pengaduan['isi_laporan'] ?></span></h5>
					
					<p class="card-text"> Status :
					<td>
							<?php
							if ($data_pengaduan['status'] == '0') {
                        ?>
                          <span class="badge badge-secondary">Sedang di verifikasi</span>
                        <?php } elseif ($data_pengaduan['status'] == 'proses') { ?>
                          <span class="badge badge-primary">Sedang di proses</span>
                        <?php } elseif ($data_pengaduan['status'] == 'selesai') { ?>
                          <span class="badge badge-success">Selesai di kerjakan</span>
                        <?php } elseif ($data_pengaduan['status'] == 'tolak') { ?>
                          <span class="badge badge-danger">Pengaduan di tolak</span>
                        <?php }
                        ?>
                      </td>
					</p>

					<p class="card-text">Tanggapan : <span class="text-success"><?= $data_pengaduan['tanggapan'] ?></span></p>

					<p class="card-text">Tgl Pengaduan : <span class="text-danger"><?= $data_pengaduan['tgl_pengaduan'] ?></span></p>
					<p class="card-text">Tgl Tanggapan : <span class="text-danger"><?= $data_pengaduan['tgl_tanggapan'] ?></span></p>
					
					
				</div>
			</div>
		</div>
	</div>

</div>
        <!-- /.container-fluid